package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Aluno aluno = new Aluno("alline","f",33,"32323232","java");
        Professor professor = new Professor("Lucas","M",26);

        professor.baterPonto();
        aluno.aplicarRegras();
        professor.acordar();
        aluno.acordar();
        professor.dormir();
    }
}
