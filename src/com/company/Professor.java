package com.company;

public class Professor extends Pessoa {
    private String especializacao;

    public Professor(String nome, String sexo, int idade) {
        super(nome, sexo, idade);
    }

    @java.lang.Override
    public void acordar() {
        System.out.println("Professor acorda as 7!");
    }

    @java.lang.Override
    public void dormir() {
        System.out.println("Professor dorme as 23!");
    }

    @java.lang.Override
    public void comer() {
        System.out.println("Professor come as 12!");
    }
}
