package com.company;

public class Funcionario extends Pessoa {

    public Funcionario(String nome, String sexo, int idade) {
        super(nome, sexo, idade);
    }

    @java.lang.Override
    public void acordar() {

    }

    @java.lang.Override
    public void dormir() {

    }

    @java.lang.Override
    public void comer() {

    }
}
