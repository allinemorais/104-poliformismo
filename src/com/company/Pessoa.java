package com.company;

//Não é possível acessar uma classe abstrata
public abstract class Pessoa {
    //Protecd somente quem herda ou quem está no mesmo pacote tem acesso
    protected String nome,sexo;
    protected int idade;

    //Metodo construtor nao precisa de void
    public Pessoa(String nome, String sexo, int idade)
    {

    }

    //Obriga as classes que herdam a imolementar o método
    public abstract void acordar();
    public abstract void dormir();
    public abstract void comer();

    public void baterPonto()
    {
        System.out.println("Ponto Batido!");
    }

    public void aplicarRegras()
    {
        System.out.println("regras aplicadas!");
    }



}
