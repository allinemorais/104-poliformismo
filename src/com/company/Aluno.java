package com.company;

public class Aluno extends Pessoa {
    private String numeroMatricula, curso;

    public Aluno(String nome, String sexo, int idade, String numeroMatricula, String curso) {
        super(nome, sexo, idade);
        this.numeroMatricula = numeroMatricula;
        this.curso = curso;
    }

    @java.lang.Override
    public void acordar() {
        System.out.println("aluno acorda as 7!");
    }

    @java.lang.Override
    public void dormir() {
        System.out.println("aluno dorme as 23!");
    }

    @java.lang.Override
    public void comer() {
        System.out.println("aluno come as 12!");
    }
}
